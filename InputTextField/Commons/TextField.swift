//
//  CustomTextField.swift
//  InputTextField
//
//  Created by Quang Công on 17/06/2021.
//

import Foundation
import UIKit

class TextField: UITextField {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        makeUI()
        commonInit()
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        makeUI()
        commonInit()
    }

    private func makeUI() {
        translatesAutoresizingMaskIntoConstraints = false
        keyboardType = .numberPad
        layer.borderColor = UIColor.systemGray4.cgColor
        textColor = .label
        tintColor = .label
        textAlignment = .left
        font = UIFont.preferredFont(forTextStyle: .title2)
        minimumFontSize = 12
        backgroundColor = .tertiarySystemBackground
        autocorrectionType = .no
        returnKeyType = .go
        clearButtonMode = .whileEditing
        placeholder = "0đ"
    }
    
    private func commonInit() {
        addTarget(self, action: #selector(textDidChange), for: .editingChanged)
        print("\(formatCurrency(22222))")
    }
    
    @objc private func textDidChange() {
        guard var tmp = self.text else {
            return
        }
        
        // remove "đ" from text in textfield
        if tmp.contains("đ") {
            tmp.removeAll { $0 == "đ" }
        }
        
        // remove "." from text in textfield
        if tmp.contains(".") {
            tmp.removeAll { $0 == "." }
        }
        
        // remove space from text in textfield
        tmp.removeAll { $0 == " " }

        // format currency text in textfield
        if let amountString = Int(tmp) {
            self.text = formatCurrency(amountString)
            print(amountString)
        }
        
        if let newPosition = self.position(from: self.endOfDocument, offset: -2) {
            self.selectedTextRange = self.textRange(from: newPosition, to: newPosition)
        }
    }
    
    /// Format price to string
    /// - Parameter price: text in textfield
    /// - Returns: price string
    private func formatCurrency(_ inputNumber: Int, symbol: String = "đ") -> String {
        let formatter = NumberFormatter()
        formatter.currencySymbol = symbol
        formatter.currencyGroupingSeparator = "."
        formatter.locale = Locale(identifier: "vn_VN")
        formatter.positiveFormat = "#,##0 ¤"
        formatter.numberStyle = .currency
        return formatter.string(from: inputNumber as NSNumber)!
    }
}
