# InputTextField

The numbers that the user enters in the field are automatically formatted to display in the VNĐ amount format.

## Demo

![Alt text](https://gitlab.com/congphan/textfield/-/blob/main/ImageDemo/Demo.png "Demo")

## Installation

**Source files**

Clone this repository or download it in zip-file. Then you will find source files under InputTextField directory. Copy them to your project.

## Usage


Import InputTextField and add it programatically to your view or add UITextField in your Storyboard, and then change the custom class to TextField

![Alt text](https://gitlab.com/congphan/textfield/-/blob/main/ImageDemo/ImportTextField.png "Custom class")

```
@IBOutlet weak var textField: TextField!
 
```


## License

InputTextField is released under the MIT license.


